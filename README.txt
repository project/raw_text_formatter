********************************************************************
D R U P A L    M O D U L E
********************************************************************
Name: Raw Text Formatter Module 
Author: Robert Castelo
Sponsor: Code Positive [www.codepositive.com]
Drupal: 6.0.x
********************************************************************
DESCRIPTION:

Adds format option to display the raw (sanitised) value of a text field.

Useful for outputting the key value of a selected option from a text select field.


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.  If you run into
problems, you should always read the INSTALL.txt that comes with the
Drupal package and read the online documentation.

	1. Place the entire directory into your Drupal
        modules/directory.

	2. Enable the module by navigating to:

	   Administer > Site building > Modules

	Click the 'Save configuration' button at the bottom to commit your
    changes.
           
       
********************************************************************
ACKNOWLEDGEMENTS






